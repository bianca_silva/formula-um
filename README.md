# formula-um

Consumo de API Formula1:

## Endpoints:

- GET
> v1/race/details

Retorna os detalhes mais importantes da corrida Formula1.

</details>

> v1/race/results

Retorna todos os resultados da corrida Formula1.


> v1/top/{value}

Retorna a quantidade de carros requisitada, ordenada do primeiro ao último.


> v1/race/position/{value}

Retorna um carro com a posição específica.


> v1/race/car/{value}

Retorna o carro específico.


> v1/race/constructor/{value}

Retorna todos os carros de uma mesma fabricante de carros.


> v1/race/status/{value}

Retorna todos os carros de acordo com o status que tiveram da corrida.
