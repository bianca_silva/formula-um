package br.com.springboot.formulaUm.response.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class RaceDetails {
	
	@JsonProperty("Series")
	private String series;
	
	@JsonProperty("Season")
	private String season;
	
	@JsonProperty("Round")
	private String round;	
	
	@JsonProperty("Results")
	private String results;

	public String getSeries() {
		return series;
	}

	public void setSeries(String series) {
		this.series = series;
	}

	public String getSeason() {
		return season;
	}

	public void setSeason(String season) {
		this.season = season;
	}

	public String getRound() {
		return round;
	}

	public void setRound(String round) {
		this.round = round;
	}

	public String getResults() {
		return results;
	}

	public void setResults(String results) {
		this.results = results;
	}

}
