package br.com.springboot.formulaUm.services;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import br.com.springboot.formulaUm.config.ConfigFormulaUm;
import br.com.springboot.formulaUm.model.Result;
import br.com.springboot.formulaUm.model.Root;
import br.com.springboot.formulaUm.response.model.RaceDetails;

@Service
public class FormulaUmService {

	@Autowired
	private RestTemplate restTemplate;

	@Autowired
	private ConfigFormulaUm config;

	private String url;

	@PostConstruct
	public void init() {
		url = config.getUrlTemplate();
	}

	public RaceDetails getRaceDetails() throws JsonParseException, JsonMappingException, IOException {
		final Root obj = clientRace();

		final RaceDetails dto = new RaceDetails();

		dto.setSeries(obj.getmRData().getSeries());
		dto.setResults(obj.getmRData().getTotal());
		dto.setSeason(obj.getmRData().getRaceTable().getSeason());
		dto.setRound(obj.getmRData().getRaceTable().getRound());

		return dto;
	}

	public List<Result> getRaceResults() throws JsonParseException, JsonMappingException, IOException {
		final Root obj = clientRace();

		return obj.getmRData().getRaceTable().getRaces().get(0).getResults();

	}

	public List<Result> getTopPosition(int value) throws JsonParseException, JsonMappingException, IOException {
		final Root obj = clientRace();
		List<Result> racesResults = obj.getmRData().getRaceTable().getRaces().get(0).getResults();
		List<Result> result = new ArrayList<Result>();

		for (int i = 0; i < value; i++) {
			result.add(racesResults.get(i));
		}

		return result;

	}

	public List<Result> getCarPosition(String position) throws JsonParseException, JsonMappingException, IOException {
		final Root obj = clientRace();
		List<Result> racesResults = obj.getmRData().getRaceTable().getRaces().get(0).getResults();
		List<Result> result = new ArrayList<Result>();

		for (int i = 0; i < racesResults.size(); i++) {
			if (racesResults.get(i).getPosition().equals(position)) {
				result.add(racesResults.get(i));
			}
		}

		return result;
	}

	public List<Result> getCar(String value) throws JsonParseException, JsonMappingException, IOException {
		final Root obj = clientRace();
		List<Result> racesResults = obj.getmRData().getRaceTable().getRaces().get(0).getResults();
		List<Result> result = new ArrayList<Result>();

		for (int i = 0; i < racesResults.size(); i++) {
			if (racesResults.get(i).getNumber().equals(value)) {
				result.add(racesResults.get(i));
			}
		}

		return result;

	}

	public List<Result> getConstructor(String name) throws JsonParseException, JsonMappingException, IOException {
		final Root obj = clientRace();
		List<Result> racesResults = obj.getmRData().getRaceTable().getRaces().get(0).getResults();
		List<Result> result = new ArrayList<Result>();

		for (int i = 0; i < racesResults.size(); i++) {
			if (racesResults.get(i).getConstructor().getName().equals(name)) {
				result.add(racesResults.get(i));
			}
		}

		return result;
	}

	public List<Result> getStatus(String status) throws JsonParseException, JsonMappingException, IOException {
		final Root obj = clientRace();
		List<Result> racesResults = obj.getmRData().getRaceTable().getRaces().get(0).getResults();
		List<Result> result = new ArrayList<Result>();

		for (int i = 0; i < racesResults.size(); i++) {
			if (racesResults.get(i).getStatus().equals(status)) {
				result.add(racesResults.get(i));
			}
		}

		return result;
	}

	public Root clientRace() throws JsonParseException, JsonMappingException, IOException {

		final ObjectMapper mapper = new ObjectMapper();
		return mapper.readValue(restTemplate.getForObject(url, String.class), Root.class);
	}
}
