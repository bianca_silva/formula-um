package br.com.springboot.formulaUm;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 *
 * Spring Boot application starter class
 */
@SpringBootApplication(scanBasePackages = Application.BASE_PACKAGE)
public class Application {
	
	public static final String BASE_PACKAGE = "br.com.springboot.formulaUm";
	
    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }
}
