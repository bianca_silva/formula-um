package br.com.springboot.formulaUm.controllers;

import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;

import br.com.springboot.formulaUm.model.Result;
import br.com.springboot.formulaUm.response.model.RaceDetails;
import br.com.springboot.formulaUm.services.FormulaUmService;


@RestController
@RequestMapping("/v1/race")
public class F1Controller {
	
	@Autowired
	private FormulaUmService formulaUmService;
    
    @GetMapping(value = "/details", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<RaceDetails> getRaceDetails() throws JsonParseException, JsonMappingException, IOException {
    	
    	RaceDetails dto = formulaUmService.getRaceDetails();
    	
    	return new ResponseEntity<RaceDetails>(dto, HttpStatus.OK);
    }
    
    @GetMapping(value = "/results", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<List<Result>> getRaceResults() throws JsonParseException, JsonMappingException, IOException {
    	
    	List<Result> dto = formulaUmService.getRaceResults();
    	
        return new ResponseEntity<List<Result>>(dto, HttpStatus.OK);
    }
    
    @GetMapping(value = "/top/{value}", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<List<Result>> getTopPosition(@PathVariable int value) throws JsonParseException, JsonMappingException, IOException {
    	
    	List<Result> result = formulaUmService.getTopPosition(value);
    	return new ResponseEntity<List<Result>>(result, HttpStatus.OK);
    }
    
    @GetMapping(value = "/position/{position}", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<List<Result>> getCarPosition(@PathVariable String position) throws JsonParseException, JsonMappingException, IOException {
    	
    	List<Result> result = formulaUmService.getCarPosition(position);
    	return new ResponseEntity<List<Result>>(result, HttpStatus.OK);
    }
    
    @GetMapping(value = "/car/{value}", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<List<Result>> getCar(@PathVariable String value) throws JsonParseException, JsonMappingException, IOException {
    	
    	List<Result> result = formulaUmService.getCar(value);
    	return new ResponseEntity<List<Result>>(result, HttpStatus.OK);
    }
    
    @GetMapping(value = "/constructor/{name}", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<List<Result>> getConstructor(@PathVariable String name) throws JsonParseException, JsonMappingException, IOException {
    	
    	List<Result> result = formulaUmService.getConstructor(name);
    	return new ResponseEntity<List<Result>>(result, HttpStatus.OK);
    }
    
    @GetMapping(value = "/status/{status}", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<List<Result>> getStatus(@PathVariable String status) throws JsonParseException, JsonMappingException, IOException {
    	
    	List<Result> result = formulaUmService.getStatus(status);
    	return new ResponseEntity<List<Result>>(result, HttpStatus.OK);
    }
    
}
