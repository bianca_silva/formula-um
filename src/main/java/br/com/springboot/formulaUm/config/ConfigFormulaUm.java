package br.com.springboot.formulaUm.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties(ConfigFormulaUm.CONFIG_PROPERTIES)
public class ConfigFormulaUm {

	protected static final String CONFIG_PROPERTIES = "formula-um";

	private String urlTemplate;

	public String getUrlTemplate() {
		return urlTemplate;
	}

	public void setUrlTemplate(String urlTemplate) {
		this.urlTemplate = urlTemplate;
	}

}
