package br.com.springboot.formulaUm.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Circuit {

	public String circuitId;
	public String url;
	public String circuitName;
	@JsonProperty("Location")
	public Location location = new Location();

	public String getCircuitId() {
		return circuitId;
	}

	public void setCircuitId(String circuitId) {
		this.circuitId = circuitId;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getCircuitName() {
		return circuitName;
	}

	public void setCircuitName(String circuitName) {
		this.circuitName = circuitName;
	}

	public Location getLocation() {
		return location;
	}

	public void setLocation(Location location) {
		this.location = location;
	}

}
