package br.com.springboot.formulaUm.model;

public class AverageSpeed {
	
	public String units;
	public String speed;
	
	public String getUnits() {
		return units;
	}
	public void setUnits(String units) {
		this.units = units;
	}
	public String getSpeed() {
		return speed;
	}
	public void setSpeed(String speed) {
		this.speed = speed;
	}
}
