package br.com.springboot.formulaUm.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Root {

	@JsonProperty("MRData")
	public MRData mRData = new MRData();

	public MRData getmRData() {
		return mRData;
	}

	public void setmRData(MRData mRData) {
		this.mRData = mRData;
	}

	public void setSeries(String series) {
		mRData.setSeries(series);		
	}

}
