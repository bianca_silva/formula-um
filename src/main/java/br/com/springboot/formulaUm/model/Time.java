package br.com.springboot.formulaUm.model;

public class Time {

	public String millis;
	public String time;

	public String getMillis() {
		return millis;
	}

	public void setMillis(String millis) {
		this.millis = millis;
	}

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}
}
