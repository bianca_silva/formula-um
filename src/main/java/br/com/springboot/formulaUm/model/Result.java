package br.com.springboot.formulaUm.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Result{
	
    public String number;
    public String position;
    public String positionText;
    public String points;
    @JsonProperty("Driver") 
    public Driver driver = new Driver();
    @JsonProperty("Constructor") 
    public Constructor constructor = new Constructor();
    public String grid;
    public String laps;
    public String status;
    @JsonProperty("Time") 
    public Time time = new Time();
    @JsonProperty("FastestLap") 
	public FastestLap fastestLap = new FastestLap();

	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	public String getPosition() {
		return position;
	}

	public void setPosition(String position) {
		this.position = position;
	}

	public String getPositionText() {
		return positionText;
	}

	public void setPositionText(String positionText) {
		this.positionText = positionText;
	}

	public String getPoints() {
		return points;
	}

	public void setPoints(String points) {
		this.points = points;
	}

	public Driver getDriver() {
		return driver;
	}

	public void setDriver(Driver driver) {
		this.driver = driver;
	}

	public Constructor getConstructor() {
		return constructor;
	}

	public void setConstructor(Constructor constructor) {
		this.constructor = constructor;
	}

	public String getGrid() {
		return grid;
	}

	public void setGrid(String grid) {
		this.grid = grid;
	}

	public String getLaps() {
		return laps;
	}

	public void setLaps(String laps) {
		this.laps = laps;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Time getTime() {
		return time;
	}

	public void setTime(Time time) {
		this.time = time;
	}

	public FastestLap getFastestLap() {
		return fastestLap;
	}

	public void setFastestLap(FastestLap fastestLap) {
		this.fastestLap = fastestLap;
	}
}
