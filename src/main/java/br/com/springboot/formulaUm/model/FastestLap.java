package br.com.springboot.formulaUm.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class FastestLap {

	public String rank;
	public String lap;
	@JsonProperty("Time")
	public Time time = new Time();
	@JsonProperty("AverageSpeed")
	public AverageSpeed averageSpeed = new AverageSpeed();

	public String getRank() {
		return rank;
	}

	public void setRank(String rank) {
		this.rank = rank;
	}

	public String getLap() {
		return lap;
	}

	public void setLap(String lap) {
		this.lap = lap;
	}

	public Time getTime() {
		return time;
	}

	public void setTime(Time time) {
		this.time = time;
	}

	public AverageSpeed getAverageSpeed() {
		return averageSpeed;
	}

	public void setAverageSpeed(AverageSpeed averageSpeed) {
		this.averageSpeed = averageSpeed;
	}
}
