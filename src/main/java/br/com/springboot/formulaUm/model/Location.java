package br.com.springboot.formulaUm.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Location {

	public String lat;
	@JsonProperty("long")
	public String longitude;
	public String locality;
	public String country;

	public String getLat() {
		return lat;
	}

	public void setLat(String lat) {
		this.lat = lat;
	}

	public String getLongger() {
		return longitude;
	}

	public void setLongger(String longger) {
		this.longitude = longger;
	}

	public String getLocality() {
		return locality;
	}

	public void setLocality(String locality) {
		this.locality = locality;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}
}
